FROM ubuntu:18.04

WORKDIR /app

COPY . /app

RUN apk add --no-cache ca-certificates openssl

RUN chmod +x cloud_sql_proxy

CMD ./cloud_sql_proxy -instances=assignment-3-264920:europe-west1:conor-sql=tcp:3306 -credential_file=assignment-3-264920-2e1efb69b7f8.json
